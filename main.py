from telethon import events
from telethon.events import StopPropagation
from telethon.tl.custom import Button, file
from telethon.client import buttons, chats

import configparser
import json
from uuid import uuid4
import requests
import re
import os
import random
import aiohttp
import functools

from init import bot, logger

__author__ = "Teemo the Yiffer"

"""
Main with all the bot commands
"""

@bot.on(events.NewMessage(pattern='/start'))
async def start(ctx):
    """Introduction to bot & commands."""
    await ctx.respond(
'''Hello! Let me walk you through my commands!

    🔸 /channels
        🔹 My SFW & NSFW channels menu!

    🔸 /bash [restricted]
        🔹 Bash CLI for the server.

    🔸 /Iris
        🔹 Quotes from my anti-furry friend.

    🔸 /dog
        🔹 Get random doggo pictures!

    🔸 /crab
        🔹 Create your own crab meme video! 
            Syntax: `/crab <WORDS>,<WORDS>`

Feel free to reach out to me (@TeemoTheYiffer) for anything!''')
    raise StopPropagation

@bot.on(events.NewMessage(pattern='/crab'))
async def crab_cmd(ctx):
    """Create your very own crab rave"""
    from cogs.crab.crab import crab
    return await crab(ctx)

@bot.on(events.NewMessage(pattern='/channels'))
async def channels(ctx):
    """Inline Keyboard that link to all of my channels"""
    await ctx.respond(await main_menu_message(), buttons=await main_menu_keyboard())

@bot.on(events.ChatAction(chats=-1001115422665))
async def new_user(ctx):
    """New user joins"""
    user = await ctx.get_user()
    print(user.stringify())

@bot.on(events.NewMessage(chats=-1001115422665))
async def discord(event):
    """Discord to Telegram connector"""
    user = await event.get_sender()
    if user.id != 955256995:
        general_chat_url = "https://discordapp.com/api/webhooks/665730433698365470/hp2az6qYenTdGmv9N2yL4xYWEKblQaF0Uc8aNQLje--zpVHCcy3uXnwhlyHAVNjAUK9x"
        # print(user.stringify())
        # print(event.stringify())
        # user_pfp = await bot.get_profile_photos(user.id, limit=1)
        # print(f"USER_PFP: {user_pfp}")
        # print(f"USER_FILE_ID: {user.photo.photo_id}")
        # file_id = user.photo.photo_id
        # newFile = bot.getFile(file_id)
        payload = {
            "embeds": [{
                "description": f"{event.text}",
                "author": {
                    "name": f"{user.username}",
                    "url": "https://t.me/teemos_yiffy_tavern"
                    # "icon_url": f"{newFile}"
                },
                "footer": {
                    "icon_url": "https://image.flaticon.com/icons/png/512/124/124019.png",
                    "text": "Sent from Teemo's Yiffy Tavern group chat."
                }
                # "fields": [
                # {
                # "name": "Name-Test",
                # "value": "",
                # "inline": "true"
                # },
                # ]
            }]
        }
        headers = {'Content-Type': 'application/json'}
        response = requests.post(
            general_chat_url, data=json.dumps(payload), headers=headers)
        return response


@bot.on(events.NewMessage(from_users='TeemoTheYiffer', pattern='/bash'))
async def bash(ctx):
    """Bash shell"""
    import subprocess
    import asyncio
    from subprocess import Popen
    import threading
    from asyncio.subprocess import PIPE, STDOUT

    arg = ctx.text.replace("/bash", "")
    proc = await asyncio.create_subprocess_shell(arg, stdin=None, stderr=STDOUT, stdout=PIPE)
    out = await proc.stdout.read()
    msg = out.decode('utf-8')
    await ctx.respond(f"```\n [Bash Input]: {arg}\n```")
    await ctx.respond(msg)


async def get_url():
    contents = requests.get('https://random.dog/woof.json').json()
    url = contents['url']
    return url


async def get_image_url():
    allowed_extension = ['jpg', 'jpeg', 'png']
    file_extension = ''
    while file_extension not in allowed_extension:
        url = await get_url()
        file_extension = re.search("([^.]*)$", url).group(1).lower()
    return url


@bot.on(events.NewMessage(pattern='/dog'))
async def dog(ctx):
    url = await get_image_url()
    chat = ctx.chat_id
    await bot.send_file(chat, url)


@bot.on(events.NewMessage(pattern='/Iris'))
async def Iris(ctx):
    """
    Randomly fetches Iris Quotes.
    """
    chat = ctx.chat_id
    # Creates a list of filenames from your folder
    imgList = os.listdir(
        "/home/teemo/.local/share/Red-DiscordBot/cogs/CogManager/cogs/yiffertools/iris_quotes")
    # Selects a random element from the list
    imgString = random.choice(imgList)
    path = "/home/teemo/.local/share/Red-DiscordBot/cogs/CogManager/cogs/yiffertools/iris_quotes/" + \
        imgString  # Creates a string for the path to the file
    with open(path, "rb") as file:  # open it as binary file
        await bot.send_file(chat, file)


@bot.on(events.CallbackQuery(pattern='Main menu'))
async def main_menu(ctx):
    await ctx.edit(text=await main_menu_message(), buttons=await main_menu_keyboard())

@bot.on(events.CallbackQuery(pattern='Memes'))
async def memes_menu(ctx):
    await ctx.edit(text=await memes_menu_message(), buttons=await memes_menu_keyboard())

@bot.on(events.CallbackQuery(data='NSFW'))
async def nsfw_menu(ctx):
    await ctx.edit(text=await nsfw_menu_message(), buttons=await nsfw_menu_keyboard())

@bot.on(events.CallbackQuery(pattern='Art Tutorials'))
async def tutorials_menu(ctx):
    await ctx.edit(text=await tutorials_menu_message(), buttons=await tutorials_menu_keyboard())

@bot.on(events.CallbackQuery(pattern='SFW'))
async def sfw_menu(ctx):
    await ctx.edit(text=await sfw_menu_message(), buttons=await sfw_menu_keyboard())


async def main_menu_message():
    return 'Please choose which channel you would like to join:'


async def memes_menu_message():
    return 'What kind of Memes do you like?'


async def nsfw_menu_message():
    return 'Lewd! What sort of yiff do ya like?'


async def tutorials_menu_message():
    return 'Oh, an artist! Maybe you will like these tutorials?'


async def sfw_menu_message():
    return 'Safe for work stuff! What will it be?'


async def main_menu_keyboard():
    keyboard = [[Button.inline('Memes')],
                [Button.inline('SFW')],
                [Button.inline('Art Tutorials')],
                [Button.inline('🔞 NSFW 🔞', data='NSFW')]]
    return keyboard


async def memes_menu_keyboard():
    keyboard = [[Button.url('Artist Memes', url='https://t.me/joinchat/AAAAAFCC6iMtuvKtwUtXIQ')],
                [Button.url('🔞 Furry Memes 🔞', url='https://t.me/teemos_furry_memes')],
                [Button.url('🔞 Regular Memes 🔞', url='https://t.me/teemos_memes')],
                [Button.inline('Main menu')]]
    return keyboard


async def nsfw_menu_keyboard():
    keyboard = [[Button.url('⚣ Gay ⚣', url='https://t.me/teemos_gay_yiff')],
                [Button.url('⚢ Lesbian ⚢', url='https://t.me/teemos_lesbian_yiff')],
                [Button.url('⚤ Straight ⚤', url='https://t.me/teemos_straight_yiff')],
                [Button.url('⚥ Bisexual ⚥', url='https://t.me/teemos_bisexual_yiff')],
                [Button.url('⚧ Pansexual ⚧', url='https://t.me/teemos_pansexual_yiff')],
                [Button.url('Comics', url='https://t.me/joinchat/AAAAAETQ6g6b-g0bcE4DHA')],
                [Button.url('Feral', url='https://t.me/joinchat/AAAAAEkk3dh9WOvY0TfmVQ')],
                [Button.url("Teemo's Personal Art", url='https://t.me/joinchat/AAAAAFQ_RwcMiAJq2PAbng')],
                [Button.inline('Main menu')]]
    return keyboard

async def tutorials_menu_keyboard():
    keyboard = [[Button.url('SFW Tutorials', url='https://t.me/joinchat/AAAAAFURBrkBMW7jTgoJjg')],
                [Button.url('🔞 NSFW Tutorials 🔞', url='https://t.me/joinchat/AAAAAEfCuLuEMtff5__CZQ')],
                [Button.inline('Main menu')]]
    return keyboard


async def sfw_menu_keyboard():
    keyboard = [[Button.url('Cute Animals', url='https://t.me/joinchat/AAAAAEf-fLjzSoeQzq9hAw')],
                [Button.url('SFW Furry Art', url='https://t.me/teemos_sfw_art')],
                [Button.url('Fursuits', url='https://t.me/joinchat/AAAAAEVm9Itf5eQfKqWLyg')],
                [Button.inline('Main menu')]]
    return keyboard

def main():
    """Start the bot."""
    bot.run_until_disconnected()

if __name__ == '__main__':
    main()
