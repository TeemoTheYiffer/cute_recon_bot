from uuid import uuid4
from telegram.ext import Updater, InlineQueryHandler, CommandHandler
from telegram.utils.helpers import escape_markdown
from telegram import InlineQueryResultArticle, InlineQueryResultPhoto, ParseMode, \
    InputTextMessageContent, ChatAction
import requests
import re
import logging
import os
import random
import aiohttp
import asyncio
import logging
import functools
from moviepy.editor import VideoFileClip, TextClip, CompositeVideoClip

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

from functools import wraps
def send_typing_action(func):
    """Sends typing action while processing func command."""

    @wraps(func)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        return func(update, context,  *args, **kwargs)

    return command_func

@send_typing_action
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('H-h-hello there~')

@send_typing_action
def Iris(update, context):
    """
    Randomly fetches Iris Quotes.
    """
    imgList = os.listdir("/home/teemo/.local/share/Red-DiscordBot/cogs/CogManager/cogs/yiffertools/iris_quotes") # Creates a list of filenames from your folder
    imgString = random.choice(imgList) # Selects a random element from the list
    path = "/home/teemo/.local/share/Red-DiscordBot/cogs/CogManager/cogs/yiffertools/iris_quotes/" + imgString # Creates a string for the path to the file
    with open (path, "rb") as file: # open it as binary file
        context.bot.send_photo(update.message.chat_id, file)

@send_typing_action
def help(update, context):
    update.message.reply_text("Use /start to test this bot.")

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)

def get_url():
    contents = requests.get('https://random.dog/woof.json').json()
    url = contents['url']
    return url

def get_image_url():
    allowed_extension = ['jpg','jpeg','png']
    file_extension = ''
    while file_extension not in allowed_extension:
        url = get_url()
        file_extension = re.search("([^.]*)$",url).group(1).lower()
    return url

@send_typing_action
def dog(update, context):
    url = get_image_url()
    chat_id = update.message.chat_id
    context.bot.send_photo(chat_id=chat_id, photo=url)

def inlinequery(update, context):
    """Handle the inline query."""
    image = get_image_url()
    results = [
        InlineQueryResultPhoto(
            id=uuid4(),
            title="Dog",
            thumb_url=image,
            photo_url=image)]

    update.inline_query.answer(results, cache_time=1)

cog_data_path = os.listdir("/home/teemo/.local/share/Red-DiscordBot/cogs/CrabRave/")

CRAB_LINK = (
    "https://github.com/DankMemer/meme-server"
    "/raw/9ce10a61e133f5b87b24d425fc671c9295affa6a/assets/crab/template.mp4"
)
# Use a historical link incase something changes
FONT_FILE = (
    "https://github.com/matomo-org/travis-scripts/"
    "raw/65cace9ce09dca617832cbac2bbae3dacdffa264/fonts/Verdana.ttf"
)

def check_video_file():
    if not (cog_data_path + "template.mp4").is_file():
        try:
            with aiohttp.ClientSession() as session:
                with session.get(CRAB_LINK) as resp:
                    data = resp.read()
            with open(cog_data_path + "template.mp4", "wb") as save_file:
                save_file.write(data)
        except Exception:
            logger.warning("Error downloading crabrave video template", exc_info=True)
            return False
    return True

def check_font_file():
    if not (cog_data_path + "Verdana.ttf").is_file():
        try:
            with aiohttp.ClientSession() as session:
                with session.get(FONT_FILE) as resp:
                    data = resp.read()
            with open(cog_data_path + "Verdana.ttf", "wb") as save_file:
                save_file.write(data)
        except Exception:
            logger.warning("Error downloading crabrave video template", exc_info=True)
            return False
    return True

def crab(update, ctx):
    """Make crab rave videos

        There must be exactly 1 `,` to split the message
    """
    # t = update.message.clean_content[len(f"{ctx.prefix}{ctx.invoked_with}"):]
    # t = t.upper().replace(", ", ",").split(",")
    if not check_video_file():
        return update.message.reply_text("I couldn't download the template file.")
    if not check_font_file():
        return update.message.reply_text("I couldn't download the font file.")
    if len(t) != 2:
        return update.message.reply_text("You must submit exactly two strings split by comma")
    if (not t[0] and not t[0].strip()) or (not t[1] and not t[1].strip()):
        return update.message.reply_text("Cannot render empty text")
    fake_task = functools.partial(make_crab, t=t, u_id=uuid4())
    task = ctx.loop.run_in_executor(None, fake_task)
    try:
        asyncio.wait_for(task, timeout=300)
    except asyncio.TimeoutError:
        logger.warning("Error generating crabrave video", exc_info=True)
        return
    fp = cog_data_path + f"{uuid4()}crabrave.mp4"
    try:
        ctx.bot.send_video(str(fp), filename="crabrave.mp4")
    except Exception:
        logger.warning("Error sending crabrave video", exc_info=True)
        pass
    try:
        os.remove(fp)
    except Exception:
        logger.warning("Error deleting crabrave video", exc_info=True)

def make_crab(update, t, u_id):
    """Non blocking crab rave video generation from DankMemer bot

    https://github.com/DankMemer/meme-server/blob/master/endpoints/crab.py
    """
    fp = str(cog_data_path + f"Verdana.ttf")
    clip = VideoFileClip(str(cog_data_path) + "/template.mp4")
    # clip.volume(0.5)
    text = TextClip(t[0], fontsize=48, color="white", font=fp)
    text2 = (
        TextClip("____________________", fontsize=48, color="white", font=fp)
        .set_position(("center", 210))
        .set_duration(15.4)
    )
    text = text.set_position(("center", 200)).set_duration(15.4)
    text3 = (
        TextClip(t[1], fontsize=48, color="white", font=fp)
        .set_position(("center", 270))
        .set_duration(15.4)
    )

    video = CompositeVideoClip(
        [clip, text.crossfadein(1), text2.crossfadein(1), text3.crossfadein(1)]
    ).set_duration(15.4)
    video = video.volumex(0.1)
    video.write_videofile(
        str(cog_data_path) + f"/{u_id}crabrave.mp4",
        threads=1,
        preset="superfast",
        verbose=False,
        logger=None,
        temp_audiofile=str(cog_data_path + f"{u_id}crabraveaudio.mp3")
        # ffmpeg_params=["-filter:a", "volume=0.5"]
    )
    clip.close()
    video.close()
    return True


def main():
    updater = Updater("955256995:AAGCXBPSl-RDx4bY7j6Pbkk0j-83bzcOV1I", use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('dog', dog))
    dp.add_handler(CommandHandler('Iris', Iris))
    dp.add_handler(CommandHandler('crab', crab))
    dp.add_handler(CommandHandler('help', help))
    # on noncommand i.e message - echo the message on Telegram
    dp.add_handler(InlineQueryHandler(inlinequery))
    # Start the Bot
    updater.start_polling()
    # Run bot until Ctrl-C or the process SIGINT, SIGTERM or SIGABRT
    updater.idle()

if __name__ == '__main__':
    main()