from uuid import uuid4
from telegram.ext import Updater, InlineQueryHandler, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram.utils.helpers import escape_markdown
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode, \
    InputTextMessageContent, ChatAction
import requests
import re
import logging
import os
import random
import aiohttp
import functools

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)

from functools import wraps
def send_typing_action(func):
    """Sends typing action while processing func command."""

    @wraps(func)
    def command_func(update, context, *args, **kwargs):
        context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=ChatAction.TYPING)
        return func(update, context,  *args, **kwargs)

    return command_func

@send_typing_action
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text(main_menu_message(),
                        reply_markup=main_menu_keyboard())

@send_typing_action
def Iris(update, context):
    """
    Randomly fetches Iris Quotes.
    """
    imgList = os.listdir("/home/teemo/.local/share/Red-DiscordBot/cogs/CogManager/cogs/yiffertools/iris_quotes") # Creates a list of filenames from your folder
    imgString = random.choice(imgList) # Selects a random element from the list
    path = "/home/teemo/.local/share/Red-DiscordBot/cogs/CogManager/cogs/yiffertools/iris_quotes/" + imgString # Creates a string for the path to the file
    with open (path, "rb") as file: # open it as binary file
        context.bot.send_photo(update.message.chat_id, file)

@send_typing_action
def help(update, context):
    update.message.reply_text("Use /start to test this bot.")

def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)

def get_url():
    contents = requests.get('https://random.dog/woof.json').json()
    url = contents['url']
    return url

def get_image_url():
    allowed_extension = ['jpg','jpeg','png']
    file_extension = ''
    while file_extension not in allowed_extension:
        url = get_url()
        file_extension = re.search("([^.]*)$",url).group(1).lower()
    return url

@send_typing_action
def dog(update, context):
    url = get_image_url()
    chat_id = update.message.chat_id
    context.bot.send_photo(chat_id=chat_id, photo=url)

def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, [header_buttons])
    if footer_buttons:
        menu.append([footer_buttons])
    return menu

def bash(update, context):
    """Bash shell"""
    from subprocess import Popen, PIPE, STDOUT
    user_input = " ".join(context.args)
    proc = Popen(user_input, shell=True, stdin=None, stderr=STDOUT, stdout=PIPE)
    out = proc.stdout.read()
    msg = out.decode('utf-8')
    update.message.reply_markdown(f"```[Input]: {user_input}\n```")
    update.message.reply_markdown(f"```{msg}```")

def main_menu(update, context):
  query = update.callback_query
  context.bot.edit_message_text(chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        text=main_menu_message(),
                        reply_markup=main_menu_keyboard())

def memes_menu(update, context):
  query = update.callback_query
  context.bot.edit_message_text(chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        text=memes_menu_message(),
                        reply_markup=memes_menu_keyboard())

def nsfw_menu(update, context):
  query = update.callback_query
  context.bot.edit_message_text(chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        text=nsfw_menu_message(),
                        reply_markup=nsfw_menu_keyboard())

def tutorials_menu(update, context):
  query = update.callback_query
  context.bot.edit_message_text(chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        text=tutorials_menu_message(),
                        reply_markup=tutorials_menu_keyboard())

def sfw_menu(update, context):
  query = update.callback_query
  context.bot.edit_message_text(chat_id=query.message.chat_id,
                        message_id=query.message.message_id,
                        text=sfw_menu_message(),
                        reply_markup=sfw_menu_keyboard())
                        
def main_menu_message():
  return 'Please choose which channel you would like to join:'

def memes_menu_message():
  return 'What kind of Memes do you like?'

def nsfw_menu_message():
  return '[18+] Lewd! What sort of yiff do ya like?'

def tutorials_menu_message():
  return 'Oh, an artist! Maybe you will like these tutorials?'

def sfw_menu_message():
  return 'Safe for work stuff! What will it be?'

def main_menu_keyboard():
  keyboard = [[InlineKeyboardButton('Memes', callback_data='memes')],
              [InlineKeyboardButton('SFW', callback_data='sfw')],
              [InlineKeyboardButton('Art Tutorials', callback_data='tutorials')],
              [InlineKeyboardButton('[18+] NSFW', callback_data='nsfw')]]
  return InlineKeyboardMarkup(keyboard)

def memes_menu_keyboard():
  keyboard = [[InlineKeyboardButton('Artist Memes', url='https://t.me/joinchat/AAAAAFCC6iMtuvKtwUtXIQ')],
              [InlineKeyboardButton('[18+] Furry Memes', url='https://t.me/teemos_furry_memes')],
              [InlineKeyboardButton('[18+] Regular Memes', url='https://t.me/teemos_memes')],
              [InlineKeyboardButton('Main menu', callback_data='main')]]
  return InlineKeyboardMarkup(keyboard)

def nsfw_menu_keyboard():
  keyboard = [[InlineKeyboardButton('⚣ Gay ⚣', url='https://t.me/teemos_gay_yiff')],
              [InlineKeyboardButton('⚢ Lesbian ⚢', url='https://t.me/teemos_lesbian_yiff')],
              [InlineKeyboardButton('⚤ Straight ⚤', url='https://t.me/teemos_memes')],
              [InlineKeyboardButton('⚥ Bisexual ⚥', url='https://t.me/teemos_bisexual_yiff')],
              [InlineKeyboardButton('⚧ Pansexual ⚧', url='https://t.me/teemos_memes')],
              [InlineKeyboardButton('Main menu', callback_data='main')]]
  return InlineKeyboardMarkup(keyboard)

def tutorials_menu_keyboard():
  keyboard = [[InlineKeyboardButton('SFW Tutorials', url='--')],
              [InlineKeyboardButton('[18+] NSFW Tutorials', url='--')],
              [InlineKeyboardButton('Main menu', callback_data='main')]]
  return InlineKeyboardMarkup(keyboard)

def sfw_menu_keyboard():
  keyboard = [[InlineKeyboardButton('Cute Animals', url='https://t.me/joinchat/AAAAAEf-fLjzSoeQzq9hAw')],
              [InlineKeyboardButton('SFW Furry Art', url='https://t.me/teemos_sfw_art')],
              [InlineKeyboardButton('Main menu', callback_data='main')]]
  return InlineKeyboardMarkup(keyboard)

def add_group(update, context):
    for member in update.message.new_chat_members:
        update.message.reply_text("{username} add group".format(username=member.username))

add_group_handle = MessageHandler(Filters.status_update.new_chat_members, add_group)

def main():
    updater = Updater("955256995:AAGCXBPSl-RDx4bY7j6Pbkk0j-83bzcOV1I", use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('dog', dog))
    dp.add_handler(CommandHandler('Iris', Iris))
    dp.add_handler(CommandHandler('help', help))
    dp.add_handler(CommandHandler('bash', bash))
    dp.add_handler(add_group_handle)
    dp.add_handler(CallbackQueryHandler(main_menu, pattern='main'))
    dp.add_handler(CallbackQueryHandler(memes_menu, pattern='memes'))
    dp.add_handler(CallbackQueryHandler(nsfw_menu, pattern='nsfw'))
    dp.add_handler(CallbackQueryHandler(tutorials_menu, pattern='tutorials'))
    dp.add_handler(CallbackQueryHandler(sfw_menu, pattern='sfw'))
    # Start the Bot
    updater.start_polling()
    # Run bot until Ctrl-C or the process SIGINT, SIGTERM or SIGABRT
    updater.idle()

if __name__ == '__main__':
    main()